// based on core momentum package plugin
//
var sideToSide2 = function(fromX, toX) {
    return function(options) {
        options = _.extend({
            duration: 1000,
            delay: 600,
            easing: 'ease-in-out'
        }, options);

        return {
            insertElement: function(node, next, done) {
                var $node = $(node);
                $node
                    .css('transform', 'translateX(' + fromX + ')')
                    .insertBefore(next)
                    .velocity({
                        translateX: [0, fromX]
                    }, {
                        delay: options.delay,
                        easing: options.easing,
                        duration: options.duration,
                        queue: false,
                        complete: done
                    });
            },
            removeElement: function(node, done) {
                var $node = $(node);
                $node
                    .velocity({
                        translateX: [toX]
                    }, {
                        duration: options.duration,
                        easing: options.easing,
                        complete: function() {
                            $node.remove();
                            done();
                        }
                    });
            }
        }
    }
}

Momentum.registerPlugin('right-to-left2', sideToSide2('200%', '-200%'));
Momentum.registerPlugin('left-to-right2', sideToSide2('-200%', '200%'));
